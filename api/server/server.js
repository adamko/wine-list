require('./config/config');

// Require start server
const path = require('path');
const http = require('http');
const express = require('express');
const socketIO = require('socket.io');
const bodyParser = require('body-parser');

const port = process.env.PORT;
const api = require('./routes/api');

// Set server option
var app = express();
var server = http.createServer(app);
var io = socketIO(server);

const distPath = path.join(__dirname, '../dist');
const publicPath = path.join(__dirname, '../public');

app.use(express.static(distPath));
app.use(express.static(publicPath));

// Parsers for POST data
app.use(bodyParser.json({limit: '50mb', uploadDir: publicPath}));
app.use(bodyParser.urlencoded({ extended: false }));

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../dist/index.html'));
});

// Set api routes
app.use('/api', api);


// Create server
server.listen(port, () => {
    console.log(`Server is up on ${port}`);
});

module.exports = { app };