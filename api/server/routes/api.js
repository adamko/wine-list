const express = require('express');
const router = express.Router();
const _ = require('lodash');

const { mongoose } = require('../db/mongoose');
const { User } = require('../models/user');
const { Menu } = require('../models/menu');
const { authenticate } = require('../middleware/authenticate');

const { Image } = require('../classes/image');

router.get('/', authenticate, (req, res) => {
    res.send('api works');
});

//------------------------------------------------------------------------------
// MENU
//------------------------------------------------------------------------------
router.post('/get-menus', authenticate, (req, res) => {
    let body = _.pick(req.body, ['id_user']);

    Menu.getAllMenus(body.id_user).then((menus) => {
        res.status(200).send(menus);
    }).catch(error => {
        res.status(400).send(error);
    });
});

router.post('/add-category', authenticate, (req, res) => {
    let body = req.body;

    if (!body.parent) delete body.parent;

    if (_.isEmpty(body.image))
        delete body.image;

    let menu = new Menu(body);

    menu.save().then((responseData) => {
        res.status(200).send(responseData);
    }).catch(error => {
        res.status(400).send(error);
    });
});

router.post('/publish-category', authenticate, (req, res) => {
    let body = req.body;
    let menu = new Menu(body);

    menu.publishMenu().then((responseData) => {
        res.status(200).send(responseData);
    }).catch(error => {
        res.status(400).send(error);
    });
});

router.post('/delete-category', authenticate, (req, res) => {
    let body = req.body.category;
    let menu = new Menu(body);

    const image = new Image(body.image);

    menu.remove().then(resData => {
        res.status(200).send(resData);
        image.deleteImage(body.id_user);
    }).catch(error => {
        res.status(400).send(error);
    });
});

//------------------------------------------------------------------------------
// AUTORIZACIA
//------------------------------------------------------------------------------

router.post('/signup', (req, res) => {
    var body = _.pick(req.body, ['email', 'password', 'admin']);
    var user = new User(body);

    user.save().then(() => user.generateAuthToken()).then((token) => {
        res.header('x-auth', token).send(user);
    }).catch((error) => {
        res.status(400).send(error);
    });
});

router.post('/login', (req, res) => {
    var body = _.pick(req.body, ['email', 'password']);

    User.findByCredentials(body.email, body.password).then((user) => {
        return user.generateAuthToken().then((token) => {
            res.header('x-auth', token).send({token, user});
        });
    }).catch((error) => {
        res.status(400).send(error);
    });
});

router.delete('/logout', authenticate, (req, res) => {
    req.user.removeToken(req.token).then(() => {
        res.status(200).send();
    }, () => {
        res.status(400).send();
    });
});

module.exports = router;