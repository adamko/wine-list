const mongoose = require('mongoose');
const MpathPlugin = require('mongoose-mpath');
const validator = require('validator');
const speakingurl = require('speakingurl');
const _ = require('lodash');

const { Image } = require('../classes/image');

var MenuSchema = new mongoose.Schema({
    id_user: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    name: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    },
    type: {
        type: Number,
        required: true
    },
    slug: {
        type: String
    },
    image: { },
    description: {
        type: String,
        trim: true
    },
    seo: {
        keywords: {
            type: String,
            trim: true,
            maxlength: 255
        },
        description: {
            type: String,
            trim: true,
            maxlength: 255
        }
    },
    publish: {
        type: Boolean,
        default: true
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

MenuSchema.plugin(MpathPlugin, {
    onDelete: 'DELETE',
    idType: mongoose.Schema.ObjectId
});

MenuSchema.statics.getAllMenus = function (id_user) {
    var Menu = this;

    return new Promise((resolve, reject) => {
        if (!id_user)
            return reject();

        Menu.getChildrenTree({
            filters: {id_user: mongoose.Types.ObjectId(id_user)},
            minLevel: 1
        }, (error, res) => {
            return resolve(_.uniqBy(res, (e) => {
                return e;
            }));
        });
    });
};

MenuSchema.methods.publishMenu = function () {
    var menu = this;

    return menu.update({
        $set: {
            publish: !menu.publish
        }
    });
};

/**
 * Vygeneruje slug z nazvu a ulozi do db
 */
MenuSchema.pre('save', function (next, done) {
    let menu = this;

    if (menu.isModified('name')) {
        menu.slug = speakingurl(menu.name);
    }

    if (menu.isModified('image')) {
        const image = new Image(menu.image);

        let imageName = image.createImageName();
        image.saveImage(menu.id_user, 'menu').then(res => {
            menu.image = imageName;
            next();
        });
    }
    else {
        next();
    }
});

var Menu = mongoose.model('Menu', MenuSchema);

module.exports = { Menu };