const fs = require('fs');
const path = require('path');
const uuidv1 = require('uuid/v1');

const { User } = require('./user');

class Image {

    constructor(image) {
        this.image = image;
    }

    saveImage(user, directory) {

        // ak neexistuje user folders vytvori ho
        new User(user).createFolder();

        return new Promise((resolve, reject) => {
            this.decodeBase64Image(this.image.base64).then(res => {

                //let filePath = path.resolve('./public/' + this.image.file.name);
                let filePath = path.resolve(`./public/${user}/${directory}/full/${this.image.file.name}`);

                fs.writeFile(filePath, res.data, 'base64', (err) => {

                    if (err) return reject(err);

                    return resolve(true);
                });
            });
        });
    }

    deleteImage(user) {
        const _user = new User(user);
        var folders = _user.getFolders();

        folders.forEach(function(element) {
            let imagePath = `${element}/${this.image}`;
            fs.unlink(imagePath, function (err) {
                if (err && err.code == "ENOEN")
                    console.log(err);
                else
                    return true;
            });
        }, this);
    }

    createImageName() {

        let extension = this.image.file.name.slice((this.image.file.name.lastIndexOf(".") - 1 >>> 0) + 2);
        let name = `${uuidv1()}.${extension}`;

        this.image.file.name = name;
        return name;
    }

    decodeBase64Image(dataString) {
        let matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
        let response = {};

        return new Promise((resolve, reject) => {
            if (matches.length !== 3) {
                reject('Invalid input string');
                return false;
            }

            response.type = matches[1];
            response.data = new Buffer(matches[2], 'base64');

            return resolve(response);
        });
    }

}

module.exports = { Image };