const fs = require('fs');
const path = require('path');

class User {
    constructor(user) {
        this.user = user;
        this.foldersTop1 = ['menu', 'products'];
        this.foldersTop2 = ['full', 'retina', 'thumb'];

        this.folders = [
            path.resolve(`./public/${this.user}`),
            path.resolve(`./public/${this.user}/${this.foldersTop1[0]}`),
            path.resolve(`./public/${this.user}/${this.foldersTop1[1]}`),
            path.resolve(`./public/${this.user}/${this.foldersTop1[0]}/${this.foldersTop2[0]}`),
            path.resolve(`./public/${this.user}/${this.foldersTop1[0]}/${this.foldersTop2[1]}`),
            path.resolve(`./public/${this.user}/${this.foldersTop1[0]}/${this.foldersTop2[2]}`)
        ];
    }

    createFolder() {
        this.folders.forEach(function(element) {
            if (!fs.existsSync(element)) {
                return fs.mkdirSync(element);
            }
        }, this);
    }

    getFolders() {
        return this.folders;
    }
}

module.exports = { User };