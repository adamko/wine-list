import { WineListAngularPage } from './app.po';

describe('wine-list-angular App', () => {
  let page: WineListAngularPage;

  beforeEach(() => {
    page = new WineListAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
