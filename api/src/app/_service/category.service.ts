import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class CategoryService {

	private auth: Object;

	constructor(private http: Http) {
		this.auth = JSON.parse(localStorage.getItem('auth'));
	}

	save(category: Object): Observable<object> {
		category["id_user"] = this.auth["_id"];
		return this.http.post('/api/add-category',
			category,
			{ headers: new Headers({'x-auth': this.auth["token"] })
		}).map((res: Response) => {
			if (res.ok)
				return res.json();

			return false;
		});
	}

	getAll(): Observable<object> {
		return this.http.post('/api/get-menus',
			{ id_user: this.auth["_id"] },
			{ headers: new Headers({'x-auth': this.auth["token"] })
		}).map((res: Response) => {
			if (res.ok) {
				return res.json();
			}

			return false;
		});
	}

	publishOne(category: Object): Observable<boolean> {
		return this.http.post('/api/publish-category',
			category,
			{ headers: new Headers({ 'x-auth': this.auth["token"] })
		}).map((res: Response) => {
			if (res.ok)
				return true;

			return false;
		});
	}

	removeOne(category: Object): Observable<object> {
		return this.http.post('/api/delete-category',
			{ category },
			{ headers: new Headers({'x-auth': this.auth["token"] })
		}).map((res: Response) => {
			if (res.ok)
				return res.json();

			return false;
		});
	}

}
