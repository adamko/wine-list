import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

    public token: string;

    public loggedInChange: BehaviorSubject<boolean>;

	constructor(private http: Http) {
        // nacita token ak uz je uzivatel prihlaseny
        let currentUser = JSON.parse(localStorage.getItem('auth'));

        this.token = currentUser && currentUser.token;
        this.loggedInChange = new BehaviorSubject<boolean>(this.token ? true : false);

        // this.loggedInChange.subscribe(res => {
        //     isLogged = res;

        //     console.log(`loggedInChange: ${res}`);
        // });
    }

    /**
     * Posle poziadavku na server s prihlasovacimi udajmi
     * @param data 
     */
	login(data: object): Observable<boolean> {
		return this.http.post('/api/login', data).map((res: Response) => {
            let resData = res.json();

            if (resData) {
                this.token = resData.token;

                // ulozi uzivatelske data do localStorage
                localStorage.setItem('auth', JSON.stringify({
                    email: resData["user"].email,
                    _id: resData["user"]._id,
                    token: resData.token
                }));

                this.loggedInChange.next(true);
                return true;
            }
            else
                return false;
        });
    }
    
    /**
     * Vymaze token z localstorage
     */
    logout(): Observable<boolean> {
        let auth = JSON.parse(localStorage.getItem('auth'));

        return this.http.delete('/api/logout', {
            headers: new Headers({'x-auth': auth.token})
        }).map((res: Response) => {
            if (!res)
                return false;
            
            localStorage.removeItem('auth');
            
            this.token = null;
            this.loggedInChange.next(false);

            return true;
        });
    }

}
