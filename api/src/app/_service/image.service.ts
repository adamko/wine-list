import { Injectable } from '@angular/core';

import * as _ from "lodash";

@Injectable()
export class ImageService {

	constructor() { }

	covertToBase64(file: Object) {
		return new Promise((resolve, reject) => {
			const reader = new FileReader();

			if (!file)
				return reject(false);
			else {
				reader.readAsDataURL(file[0]);
				reader.onload = (e: Event & { target: { result: string } }) => {
					return resolve({base64: reader.result, file: _.pick(file[0], ['name', 'size', 'type'])});
				};
			}
		});
	}

}
