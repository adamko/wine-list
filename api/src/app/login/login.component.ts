import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../_service/auth.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	constructor(
        private router: Router,
        private authService: AuthService
    ) { }

	ngOnInit() {
	}

	onSubmit(data: object) {
       

		// console.log(data);
		this.authService.login(data).subscribe(res => {
            if (res)
                this.router.navigate(['/dashboard']);
		});
	}

}
