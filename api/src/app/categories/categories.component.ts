import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as _ from "lodash";

import { CategoryService } from '../_service/category.service';
import { ImageService } from '../_service/image.service';

@Component({
	selector: 'app-categories',
	templateUrl: './categories.component.html',
	styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

	public categories;
	public categoriesOption: Array<any>;
    public successSave: Boolean;
	public generatePhoto: Boolean;
	public categoryType: Array<string>;

	private image: Object;
	private imageCover: Object;

	constructor(
        private categoryService: CategoryService,
        private imageService: ImageService,
		private modalService: NgbModal
	) {
		this.categories = [];
		this.categoriesOption = [];
		this.image = {};
        this.imageCover = {};

		this.generatePhoto = false;

		this.categoryType = [
			'Abstraktné menu',
			'Produktové menu'
		];
	}

	ngOnInit() {
		this.loadCategories();
	}

	/**
	 * Nacitanie vsetkych kategorii
	 */
	private loadCategories() {
		return new Promise((resolve, reject) => {
			this.categoryService.getAll().subscribe(res => {
				this.categories = res;
				console.log(this.categories);
				return resolve();
			});
		});
	}

	/**
	 * Otvori modalne okno
	 * @param content
	 */
	public openNewCategory(content): void {
		this.categoriesOption = [];
		this.recursiveCompact(this.categories);
		this.modalService.open(content, { backdrop: "static", size: "lg" });

		console.log(this.categoriesOption);
	}

	/**
	 * Ulozenie noveho menu
	 * @param data
	 */
	public onSubmit(form: any): void {

        let category = form.value;
		category.image = this.image;

		console.log('SAVE CATEGORY: ', category);
		this.categoryService.save(category).subscribe(res => {
            if (!res) return false;
			this.loadCategories().then(res => {
                this.categoriesOption = [];
                this.successSave = true;
                this.recursiveCompact(this.categories);
                form.reset();
			});
		});
	}

	public fileSelected(image): void {
        this.imageService.covertToBase64(image).then(res => {
            console.log(res);
            if (res)
                this.image = res;
        });
	}

	/**
	 * Zobrazi / vypne kategoriu
	 * @param category
	 */
	public publishCategory(category: Object): void {
		this.categoryService.publishOne(category).subscribe(res => {
			this.loadCategories();
		});
	}

	/**
	 * Vymaze kategoriu
	 * @param category
	 */
	public remove(category: Object): void {

		let confrimBox = confirm(`Naozaj chcete vymazať kategóriu ${category["name"]}?`);

		if (confrimBox) {
			this.categoryService.removeOne(category).subscribe(res => {
				if (res) {
					this.loadCategories();
				}
			});
		}
	}

	private generateSelectNested(value) {
		let optionString: string = "--";
		for (let i = 1; i < value; i++) {
			optionString += optionString;
		}
		return optionString;
	}


	private recursiveCompact(category: Array<any>): void {
		_.forEach(category, (element) => {
			if (element.type == 1) return;
			let elementPath: Array<any> = element.path.split("#");
			element._nested = this.generateSelectNested(elementPath.length);
			this.categoriesOption.push(element);
			if (element.children && element.children.length > 0) {
				this.recursiveCompact(element.children);
			}
		});
    }

}
