import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate() {
        if (localStorage.getItem('auth')) {
            return true;
        }

        // Ak uzivatel nieje prihlaseny vrati ho na login page
        this.router.navigate(['/login']);
        return false;
    }
}