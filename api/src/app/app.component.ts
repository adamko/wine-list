import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './_service/auth.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
    title = 'app works!';

    // get isLogged(): boolean {
    //     return this.authService.isLogged;
    // }

    private isLogged: boolean;
    private subscription;

    public loginAccount;

    constructor(
        private authService: AuthService,
        private router: Router
    ) {}

    ngOnInit() {
        this.subscription = this.authService.loggedInChange.subscribe(res => {
            this.isLogged = res;
            this.loginAccount = JSON.parse(localStorage.getItem('auth'));

            if (!res)
                this.router.navigate(['/login']);
        });
    }

    logout() {
        this.authService.logout().subscribe(res => {
            this.router.navigate(['/login']);
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
