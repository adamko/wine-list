import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { routing } from './app.routing';

// Providers
import { AuthService } from './_service/auth.service';
import { CategoryService } from './_service/category.service';
import { ImageService } from './_service/image.service';

// Components
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './_guards/auth.guard';
import { CategoriesComponent } from './categories/categories.component';
import { ProductsComponent } from './products/products.component';

@NgModule({
	declarations: [
        AppComponent,
        LoginComponent,
        DashboardComponent,
        CategoriesComponent,
        ProductsComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
        HttpModule,
        routing,
        NgbModule.forRoot()
	],
	providers: [
        AuthGuard,
        AuthService,
        CategoryService,
        ImageService
    ],
	bootstrap: [AppComponent]
})
export class AppModule { }
