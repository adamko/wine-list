import { Routes, RouterModule } from '@angular/router';

// Components
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CategoriesComponent } from './categories/categories.component';
import { ProductsComponent } from './products/products.component';

import { AuthGuard } from './_guards/auth.guard';

const appRoutes: Routes = [
    // { path: '/', component: AppComponent },
    // { path: 'login', component: LoginComponent },
    // { path: 'dashboard', component: DashboardComponent, children: [
    //     { path: 'categories', component: CategoriesComponent },
    //     { path: 'products', component: ProductsComponent }
    // ], canActivate: [AuthGuard] },
    
    // // Nezname url redirect
    // { path: '**', redirectTo: '' }
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'categories',
        component: CategoriesComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'products',
        component: ProductsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: '**',
        redirectTo: ''
    }
];

export const routing = RouterModule.forRoot(appRoutes);